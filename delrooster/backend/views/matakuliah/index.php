<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Personalisasi;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MatakuliahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personalisasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matakuliah-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- <?= Html::a('Create Matakuliah', ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    <?=Html::beginForm(['create'],'post');?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_matakuliah',
            'kode_matakuliah',
            'matakuliah',
            'sks',
            //'sesi',
            [
                'attribute'=>'id_dosen',
                'value'=>'dosen.nama_dosen'
            ],
            [
            'class' => 'yii\grid\CheckboxColumn'
            ],
            
            //'id_kelas',
            //'singkatan',
            //'id_sesi',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>


    <center><?=Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
     </center>
    <?= Html::endForm();?>