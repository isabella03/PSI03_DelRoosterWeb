<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Matakuliah */

$this->title = 'Create Matakuliah';
$this->params['breadcrumbs'][] = ['label' => 'Matakuliahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matakuliah-create">

<!-- <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
 --> <!--    <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<!-- </div>
</div>
</div>
</section>
</div> -->
