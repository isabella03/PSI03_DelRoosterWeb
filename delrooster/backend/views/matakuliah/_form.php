<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartik\form\ActiveForm;
//use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model backend\models\Matakuliah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="matakuliah-form">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>
                  
    <?= $form->field($model, 'kode_matakuliah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'matakuliah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sks')->textInput() ?>

    <!-- <?= $form->field($model, 'sesi')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'id_dosen')->textInput() ?>

    <?= $form->field($model, 'id_kelas')->textInput() ?>

    <?= $form->field($model, 'singkatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_sesi')->textInput() ?>

    <!-- <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div> -->
    <?php ActiveForm::end(); ?>


</div>
</div>
</div>
</div>
</section>
</div>

