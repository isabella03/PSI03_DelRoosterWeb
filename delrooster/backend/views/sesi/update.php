<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Sesi */

$this->title = 'Update Sesi: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Sesis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sesi, 'url' => ['view', 'id' => $model->id_sesi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sesi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
