<?php
use yii\helpers\Html;
use backend\models\site;
use backend\models\Jadwal;
USE yii\data\ActiveDataProvider;
use yii\grid\GridView;
// use backend\controllers\site; 


/* @var $this yii\web\View */

$this->title = 'Rooster';
?>
<div class="site-index">
  <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
<!-- <ul class="nav nav-tabs">
  <li class="active"><?= Html::a('Monday', ['/site/monday']) ?></li>
  <li><?= Html::a('Tuesday') ?></li>
  <li><?= Html::a('Wednesday') ?></li>
  <li><?= Html::a('Thursday') ?></li>
  <li><?= Html::a('Friday') ?></li>
  <li><?= Html::a('Saturday') ?></li>
</ul> -->


  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#Monday">Monday</a></li>
    <li><a data-toggle="tab" href="#Tuesday">Tuesday</a></li>
    <li><a data-toggle="tab" href="#Wednesday">Wednesday</a></li>
    <li><a data-toggle="tab" href="#Thursday">Thursday</a></li>
    <li><a data-toggle="tab" href="#Friday">Friday</a></li>
    <li><a data-toggle="tab" href="#Saturday">Saturday</a></li>
  </ul>

  <div class="tab-content">
    <div id="Monday" class="tab-pane fade in active">
      <h3>Monday</h3>
      <p>
      <?php
        $query = Jadwal::find()->where(["hari"=>"Senin"]);
        ?>
      <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'=>$query,
          ]),
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Sesi',
                'value' => 'sesi',
            ],
            [
                'attribute' => 'Hari',
                'value' => 'hari'
            ],
            [
                'attribute' => 'Ruangan',
              'value'=>  'ruangan'
            ],
            [
                'attribute' => 'Kelas',
                'value' => 'kelas'
            ],
            [
                'attribute' => 'Matakuliah',
                'value' => 'singkatan_matakuliah'
            ],
            [
                'attribute' => 'Inisial Dosen',
                'value' => 'dosen'
            ],
        ],
    ]); ?></p>
    </div>
    <div id="Tuesday" class="tab-pane fade">
      <h3>Tuesday</h3>
      <p><?php
        $query = Jadwal::find()->where(["hari"=>"Selasa"]);
        ?>
      <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'=>$query,
          ]),
        // 'filterModel' => $searchModel,
         'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Sesi',
                'value' => 'sesi',
            ],
            [
                'attribute' => 'Hari',
                'value' => 'hari'
            ],
            [
                'attribute' => 'Ruangan',
              'value'=>  'ruangan'
            ],
            [
                'attribute' => 'Kelas',
                'value' => 'kelas'
            ],
            [
                'attribute' => 'Matakuliah',
                'value' => 'singkatan_matakuliah'
            ],
            [
                'attribute' => 'Inisial Dosen',
                'value' => 'dosen'
            ],
        ],
    ]); ?></p>
    </div>
    <div id="Wednesday" class="tab-pane fade">
      <h3>Wednesday</h3>
      <p><?php
        $query = Jadwal::find()->where(["hari"=>"Rabu"]);
        ?>
      <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'=>$query,
          ]),
        // 'filterModel' => $searchModel,
         'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Sesi',
                'value' => 'sesi',
            ],
            [
                'attribute' => 'Hari',
                'value' => 'hari'
            ],
            [
                'attribute' => 'Ruangan',
              'value'=>  'ruangan'
            ],
            [
                'attribute' => 'Kelas',
                'value' => 'kelas'
            ],
            [
                'attribute' => 'Matakuliah',
                'value' => 'singkatan_matakuliah'
            ],
            [
                'attribute' => 'Inisial Dosen',
                'value' => 'dosen'
            ],
        ],
    ]); ?></p>
    </div>
    <div id="Thursday" class="tab-pane fade">
      <h3>Thursday</h3>
      <p><?php
        $query = Jadwal::find()->where(["hari"=>"Kamis"]);
        ?>
      <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'=>$query,
          ]),
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Sesi',
                'value' => 'sesi',
            ],
            [
                'attribute' => 'Hari',
                'value' => 'hari'
            ],
            [
                'attribute' => 'Ruangan',
              'value'=>  'ruangan'
            ],
            [
                'attribute' => 'Kelas',
                'value' => 'kelas'
            ],
            [
                'attribute' => 'Matakuliah',
                'value' => 'singkatan_matakuliah'
            ],
            [
                'attribute' => 'Inisial Dosen',
                'value' => 'dosen'
            ],
        ],
    ]); ?></p>
    </div>
    <div id="Friday" class="tab-pane fade">
      <h3>Friday</h3>
      <p><?php
        $query = Jadwal::find()->where(["hari"=>"Jumat"]);
        ?>
      <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'=>$query,
          ]),
        // 'filterModel' => $searchModel,
         'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Sesi',
                'value' => 'sesi',
            ],
            [
                'attribute' => 'Hari',
                'value' => 'hari'
            ],
            [
                'attribute' => 'Ruangan',
              'value'=>  'ruangan'
            ],
            [
                'attribute' => 'Kelas',
                'value' => 'kelas'
            ],
            [
                'attribute' => 'Matakuliah',
                'value' => 'singkatan_matakuliah'
            ],
            [
                'attribute' => 'Inisial Dosen',
                'value' => 'dosen'
            ],
        ],
    ]); ?></p>
    </div>
    <div id="Saturday" class="tab-pane fade">
      <h3>Saturday</h3>
      <p><?php
        $query = Jadwal::find()->where(["hari"=>"Sabtu"]);
        ?>
      <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'=>$query,
          ]),
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Sesi',
                'value' => 'sesi',
            ],
            [
                'attribute' => 'Hari',
                'value' => 'hari'
            ],
            [
                'attribute' => 'Ruangan',
              'value'=>  'ruangan'
            ],
            [
                'attribute' => 'Kelas',
                'value' => 'kelas'
            ],
            [
                'attribute' => 'Matakuliah',
                'value' => 'singkatan_matakuliah'
            ],
            [
                'attribute' => 'Inisial Dosen',
                'value' => 'dosen'
            ],
        ],
    ]); ?></p>
    </div>
  </div>

    <!-- <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content"> -->

       <!--  <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div> -->

   <!--  </div> -->
</div>
</div>
</div>
</div>
</section>
</div>
