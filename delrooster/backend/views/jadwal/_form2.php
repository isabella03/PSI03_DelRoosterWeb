<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Jadwal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- <?= $form->field($model, 'sesi')->textInput(['maxlength' => true]) ?>
 -->
    <?= $form->field($model, 'hari')->textInput(['maxlength' => true]) ?>
     <?= $form->field($model, 'hari')->dropDownList(ArrayHelper::map(Jadwal::find()->all(),'hari','jadwal'),
        [
                'prompt'=>'Pilih Hari',
        ]

); ?>

    <!-- <?= $form->field($model, 'id_ruangan')->textInput() ?> -->

   <!--  <?= $form->field($model, 'id_kelas')->textInput() ?>

    <?= $form->field($model, 'id_matakuliah')->textInput() ?>

    <?= $form->field($model, 'id_dosen')->textInput() ?>

    <?= $form->field($model, 'id_personalisasi')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
