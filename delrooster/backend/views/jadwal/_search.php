<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\JadwalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'globalSearch') ?>

<!--     <?= $form->field($model, 'id_jadwal') ?>

    <?= $form->field($model, 'hari') ?> -->

   <!--  <?= $form->field($model, 'id_ruangan') ?>

    <?= $form->field($model, 'id_kelas') ?>

    <?php  echo $form->field($model, 'id_matakuliah') ?>

    <?php  echo $form->field($model, 'id_dosen') ?> -->

    <?php // echo $form->field($model, 'id_personalisasi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
<!--         <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?> -->
<?php Html::a('Back',['index'],['class'=>'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
