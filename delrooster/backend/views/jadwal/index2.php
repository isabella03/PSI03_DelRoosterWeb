<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\JadwalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jadwal Del Rooster';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jadwal-index">
<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php echo $this->render('_search2', ['model' => $searchModel]); ?>
   
    <!-- <p>
        <?= Html::a('Create Jadwal', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
<br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_jadwal',
            [
                //'sesi',
                'attribute' => 'Sesi',
                'value' => 'sesi',
            ],
            [
                'attribute' => 'Hari',
                'value' => 'hari'
            ],
            
            // 'hari',
            // 'nomor_ruangan',
            // 'kelas',
            // 'Kode_Matakuliah',
            // 'inisial_dosen',
            // 'nama_dosen',

            [
            //'id_ruangan',
                'attribute' => 'Ruangan',
              'value'=>  'ruangan.nomor_ruangan'
            ],
            [
                'attribute' => 'Kelas',
                'value' => 'kelas.kelas'
            ],
            [
                'attribute' => 'Matakuliah',
                'value' => 'matakuliah.matakuliah'
            ],
            [
                'attribute' => 'Inisial Dosen',
                'value' => 'dosen.inisial_dosen'
            ],
            // [
            //     'attribute' => 'Nama Dosen',
            //     'value' => 'dosen.nama_dosen'
            // ],
            // 'id_kelas',
            // 'id_matakuliah',
            // 'id_dosen',
            // 'id_personalisasi',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>
</div>
</section>
</div>

