<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Personalisasi */

$this->title = 'Update Personalisasi: ' . $model->id_personalisasi;
$this->params['breadcrumbs'][] = ['label' => 'Personalisasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_personalisasi, 'url' => ['view', 'id' => $model->id_personalisasi]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="personalisasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
