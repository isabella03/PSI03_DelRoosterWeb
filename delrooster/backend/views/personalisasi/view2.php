<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\models\DetailPersonalisasi;
use backend\models\Matakuliah;
use backend\models\Akun;


/* @var $this yii\web\View */
/* @var $model backend\models\Personalisasi */

$this->title = $model->id_personalisasi;
$this->params['breadcrumbs'][] = ['label' => ' Detail Personalisasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$akun = Akun::findOne(['id_akun'=> $model->id_akun]);
?>
<div class="personalisasi-view">
    <h2 align="center"> Detail Personalisasi</h2>
    <?= GridView::widget([
        'dataProvider'=>$dataProviderDetailPersonalisasi,
        'columns'=> [
                'attribute' => 'Matakuliah',
                'format' => 'raw',
                'value'=>function($model){
                    $detail= DetailPersonalisasi::find()->where(['id_personalisasi'=>$model->id_personalisasi])->one();
                    $matakuliah = Matakuliah::find()->where(['id_matakuliah'=>$detail->id_matakuliah])->one();
                    return $matakuliah['matakuliah'];
                }
        ],
        ])

    ?>

</div>
