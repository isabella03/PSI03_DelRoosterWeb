<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Akun;
use backend\models\Matakuliah;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PersonalisasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personalisasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personalisasi-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Personalisasi', ['matakuliah/index'], ['class' => 'btn btn-success']) ?>
    </p>
    <br>
<h4 style="text-align: center;font-family: 'comic sans'">Detail Personalisasi </h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // [
            //     'attribute' => 'Id Personalisasi',
            //     'value' => 'id_personalisasi'
            // ],
            [
                'attribute' => 'Akun',
                'value' => function($model){
                    $akun = Akun::find()->where(['id_akun'=>$model->id_akun])->one();
                    return $akun->username;
                },
            ],
            [
                'attribute' => 'Create',
                'value' => 'lastUpdate'
                
            ],
            [
                'header' => '<center>Action</center>',
                'format' => 'raw',
                'value' => function ($model){
                    return '<div>'.'<center>'. Html::a('View',[
                        'personalisasi/view','id'=>$model->id_personalisasi],
                        ['class' => 'btn btn-success']) 
                    .'</center>'.'</div>'
                    ;
        // ]) ;
                },
            ],
        ],
    ]); ?>
</div>
