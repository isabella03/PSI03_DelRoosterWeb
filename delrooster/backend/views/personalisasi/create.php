<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Personalisasi */

$this->title = 'Create Personalisasi';
$this->params['breadcrumbs'][] = ['label' => 'Personalisasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personalisasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
