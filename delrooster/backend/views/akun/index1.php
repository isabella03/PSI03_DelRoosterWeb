<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AkunSearch */
/* @var $searchModel backend\models\RoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Import Akun';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akun-index">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                       
                         <p>
                              <?= Html::a('Import Akun', ['create'], ['class' => 'btn btn-success']) ?>
                         </p>
                        
                    </div>
                    
                </div>
                
            </div>
        
    </section>
</div>

   
