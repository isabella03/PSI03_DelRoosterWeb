<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Akun */

$this->title = 'Tambah Akun';
$this->params['breadcrumbs'][] = ['label' => 'Akuns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akun-create">
	<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">

<!--     <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('fromcreate', [
        'model' => $model,
         // 'role'=>$role
    ]) ?>

</div>
</div>
</div>
</div>
</section>
</div>

