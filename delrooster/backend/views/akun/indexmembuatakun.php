<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AkunSearch */
/* @var $searchModel backend\models\RoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tambah Akun';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akun-index">


<div class="akun-index">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <p>
                             
                             <p>
        <?= Html::a('Create Akun', ['create'], ['class' => 'btn btn-success']) ?>
    </p> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id_akun',
            'username',
            // 'password',
            'email:email',
            'status',
            // 'file_import',
            [ 
                'attribute'=>'Role',
                'value'=>'role.role',
                'filter'=> Html::textInput('id_role',null,['class'=>'form-control'])
                // 'filter' => Html::activeDropDownList($searchModel, 'role_id',$role,
                // ['class'=>'form-control'])
            ],

            // ['class' => 'yii\grid\ActionColumn',

            // 'template'=>'{update}   {delete}',

            // 'buttons'=> [
            //     'update' => function ($url, $model) {
            //     return Html::a('<span class="btn btn-success btn-sm"></span>', $url, [
            //                 'title' => Yii::t('yii', 'Lakukan Update'),
            //     ]);
            // },

            // ]

            // ],


[
'header' => '<center>Action</center>',
// 'attribute' => 'Action',
'format' => 'raw',
'value'=> function($model){

    return '<div>'.'<center>'. Html::a('Update', ['akun/view', 'id'=>$model->id_akun], ['class'=> 'btn btn-success'])

    // .'&nbsp;'.
    //  Html::a('Delete', ['akun/view', 'id'=>$model->id_akun], ['class'=> 'btn btn-danger','data'=>[
    //     'confirm' => 'Anda yakin untuk menghapus akun ini?',
    //     'method' =>'post',

    //  ]])
    .'</center>'.'</div>'
    ;
}


],


        ],
    ]); ?>
                        </p>
                        
                    </div>
                    
                </div>
                
            </div>
        
    </section>
</div>



    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

   <!--  <div class="row">
        <div class="col-md-3">
        </div>
    <div class="col-md-7">
    <?php //echo $this->render('_search', ['model' => $searchModel]);
    ?>
</div>
 <div class="col-md-3"> -->
    <?php //echo $this->render('_search', ['model' => $searchModel]);
    ?>
<!-- </div>
</div> -->


   
</div>
