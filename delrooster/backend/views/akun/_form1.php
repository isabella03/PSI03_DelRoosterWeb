<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Akun */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akun-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'file')->fileInput() ?>
    <?php
    if($model->file_import){
        echo 'img src="'.\Yii::$app->urlManagerFrontend->baseUrl.'/'.$model->file_import.'"width="90px">&nbsp;&nbsp;&nbsp';
        echo Html::a('delete file',['deleteimg','id'=>$model->id,'field'=>'file_import'],['class'=>'btn btn-danger']).'<p>';
    } ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

















































