<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\settings\models\Akun */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="Akun-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    <br>
   <?= $form->field($model,'file')->fileInput() ?>
    <br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'import' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
