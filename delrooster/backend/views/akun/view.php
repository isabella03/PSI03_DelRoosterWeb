<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Akun */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Akuns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akun-view">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">

  <!--   <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a('Back', ['index', 'id' => $model->id_akun], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id_akun], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_akun], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Anda Yakin?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id_akun',
            'username',
            // 'password',
            'email:email',
            'status',
            'role.role',
        ],
    ]) ?>

</div>
</div>
</div>
</div>
</section>
</div>
