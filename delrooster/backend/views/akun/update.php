<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Akun */

$this->title = 'Update';
$this->params['breadcrumbs'][] = ['label' => 'Akuns', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_akun, 'url' => ['view', 'id' => $model->id_akun]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="akun-update">
	<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('formupdate', [
        'model' => $model,
        'role'=>$role
    ]) ?>

</div>
</div>
</div>
</div>
</section>
</div>
