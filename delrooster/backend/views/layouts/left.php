
<aside class="main-sidebar">

    <section class="sidebar">
<?php
use backend\models\Akun;
$user  = Akun::findOne([Yii::$app->user->id]);
 ?>

<?php if ($user['id_role']==1) { ?>
<?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Del Rooster', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']], 
                    ['label' => 'Home', 'icon' => 'home', 'url' => ['/site/index']],                 
                    ['label' => 'Search', 'icon' => 'search', 'url' => ['/jadwal/index']],
                    // ['label' => 'Personalization', 'icon' => 'user', 'url' => ['/personalisasi/index','status'=>'view']],
                    // ['label' => 'Publication', 'icon' => 'newspaper-o', 'url' => ['/jadwal/import']],
                    ['label' => 'Registration', 'icon' => 'user-plus', 'url' => ['/akun/import']],
                    ['label' => 'Revoke', 'icon' => 'user-times', 'url' => ['/akun/index']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>
   <?php } else if ($user['id_role']==2) {?>

<?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Del Rooster', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']], 
                    ['label' => 'Home', 'icon' => 'home', 'url' => ['/site/index']],                 
                    ['label' => 'Search', 'icon' => 'search', 'url' => ['/jadwal/index']],
                    // ['label' => 'Personalization', 'icon' => 'user', 'url' => ['/personalisasi/index','status'=>'view']],
                    ['label' => 'Publication', 'icon' => 'newspaper-o', 'url' => ['/jadwal/import']],
                    // ['label' => 'Registration', 'icon' => 'user-plus', 'url' => ['/akun/import']],
                    // ['label' => 'Revoke', 'icon' => 'user-times', 'url' => ['/akun/index']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>
   <?php }  else if ($user['id_role']==3) {?>
<?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Del Rooster', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']], 
                    ['label' => 'Home', 'icon' => 'home', 'url' => ['/site/index']],                 
                    ['label' => 'Search', 'icon' => 'search', 'url' => ['/jadwal/index']],
                    ['label' => 'Personalization', 'icon' => 'user', 'url' => ['/personalisasi/index','status'=>'view']],
                    // ['label' => 'Publication', 'icon' => 'newspaper-o', 'url' => ['/jadwal/import']],
                    // ['label' => 'Registration', 'icon' => 'user-plus', 'url' => ['/akun/import']],
                    // ['label' => 'Revoke', 'icon' => 'user-times', 'url' => ['/akun/index']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>
   <?php }  else { ?>
<?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Del Rooster', 'options' => ['class' => 'header']],
                    // ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']], 
                    ['label' => 'Home', 'icon' => 'home', 'url' => ['/site/index']],                 
                    ['label' => 'Search', 'icon' => 'search', 'url' => ['/jadwal/index']],
                    // ['label' => 'Personalization', 'icon' => 'user', 'url' => ['/personalisasi/index']],
                    // ['label' => 'Publication', 'icon' => 'newspaper-o', 'url' => ['/jadwal/import']],
                    // ['label' => 'Registration', 'icon' => 'user-plus', 'url' => ['/akun/import']],
                    // ['label' => 'Revoke', 'icon' => 'user-times', 'url' => ['/akun/index']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        )?> 
   <?php } 

        // <?= dmstr\widgets\Menu::widget(
        //     [
        //         'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
        //         'items' => [
                    // ['label' => 'Del Rooster', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']], 
                //     ['label' => 'Home', 'icon' => 'home', 'url' => ['/site/index']],                 
                //     ['label' => 'Search', 'icon' => 'search', 'url' => ['/jadwal/index']],
                //     ['label' => 'Personalization', 'icon' => 'user', 'url' => ['/personalisasi/index']],
                //     ['label' => 'Publication', 'icon' => 'newspaper-o', 'url' => ['/jadwal/import']],
                //     ['label' => 'Registration', 'icon' => 'user-plus', 'url' => ['/akun/import']],
                //     ['label' => 'Revoke', 'icon' => 'user-times', 'url' => ['/akun/index']],
                //     ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                // ],
            // ]
        // ) 
   
   ?>

   
   ?>

    </section>

</aside>
