<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DetailPersonalisasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detail Personalisasis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-personalisasi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Detail Personalisasi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'detail_personalisasi_id',
            'id_personalisasi',
            'id_matakuliah',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
