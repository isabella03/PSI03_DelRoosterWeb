<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DetailPersonalisasi */

$this->title = 'Update Detail Personalisasi: ' . $model->detail_personalisasi_id;
$this->params['breadcrumbs'][] = ['label' => 'Detail Personalisasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->detail_personalisasi_id, 'url' => ['view', 'id' => $model->detail_personalisasi_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="detail-personalisasi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
