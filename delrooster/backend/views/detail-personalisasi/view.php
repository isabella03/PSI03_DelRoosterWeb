<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DetailPersonalisasi */

$this->title = $model->detail_personalisasi_id;
$this->params['breadcrumbs'][] = ['label' => 'Detail Personalisasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-personalisasi-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->detail_personalisasi_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->detail_personalisasi_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'detail_personalisasi_id',
            'id_personalisasi',
            'id_matakuliah',
        ],
    ]) ?>

</div>
