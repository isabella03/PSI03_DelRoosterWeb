<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DetailPersonalisasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-personalisasi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_personalisasi')->textInput() ?>

    <?= $form->field($model, 'id_matakuliah')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
