<?php

namespace backend\controllers;

use Yii;
use backend\models\Akun;
use backend\models\AkunSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper; 
use backend\model\Role;
use backend\models\RoleSearch;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\db\Expression;
/**
 * AkunController implements the CRUD actions for Akun model.
 */
class AkunController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Akun models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AkunSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionImport(){
        $modelImport = new \yii\base\DynamicModel([
                    'fileImport'=>'File Import',
                ]);
        $modelImport->addRule(['fileImport'],'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);
 
        if(Yii::$app->request->post()){
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            if($modelImport->fileImport && $modelImport->validate()){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow = 5;
                while(!empty($sheetData[$baseRow]['B'])){
                    $model = new \backend\models\Akun;
                    // $model1 = new \backend\models\Role;
                    $model->username = (string)$sheetData[$baseRow]['B'];
                    $model->password = (string)$sheetData[$baseRow]['C'];
                    $model->email = (string)$sheetData[$baseRow]['D'];
                    $model->status = (string)$sheetData[$baseRow]['E'];
                    // $model1->role = (string)$sheetData[$baseRow]['F'];
                    $model->save();
                    // $model1->save();
                    $baseRow++;
                }
                Yii::$app->getSession()->setFlash('success','Success');
            }else{
                Yii::$app->getSession()->setFlash('error','Error');
            }
        }
 
        return $this->render('import',[
                'modelImport' => $modelImport,
            ]);
    }


    /**
     * Displays a single Akun model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Akun model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Akun();
    //     $role = \backend\models\Role::find()->all();
    //     $role = ArrayHelper::map($role,'id_role','role');

    //     if ($model->load(Yii::$app->request->post())) { 

    //         $model->file=UploadedFile::getInstance($model,'file');
    //         $uploadExists=0;
    //         if($model->file){
    //             $imagepath='uploads/files/';
    //             $model->file_import=$imagepath .rand(10,100).'-'.str_replace('','-',$model->file->name);

    //             $bulkInsertArray=array();
                // $random_date=Yii::$app->formatter->asDatetime(date("dmyyhis"),"php:dmYHis");
                // $random=$random_date.rand(10,100);
                // $userId=\Yii::$app->akun->identity->id;
                // $now= new Expression('NOW()');

            //     $uploadExists=1;
            // }

            // if($uploadExists){
            //     $model->file->saveAs($model->file_import);

            //     $handle=fopen($model->file_import,'r');

            //     if($handle){
            //         $model->akun= $random;
            //         #var_dump($model->errors);

            //         while (($line=fgetcsv($handle,1000,",")) !=FALSE) {
            //             $bulkInsertArray[]=[
            //                 'username' => $line[0],
            //                 'password' => $line[1],
            //                 'email' => $line[2],
            //                 'status' => $line[3],
            //                 'id_role' => $line[4],
            //             ];
            //         }
            //     }
            //     fclose($handle);
            //     $tableName='akun';
            //     $columnNameArray=['username','password','email','status','id_role',];
            //     Yii::$app->db->createCommand()->akunInsert($tableName,$columnNameArray,$bulkInsertArray)->execute();
                #print_r($bulkInsertArray);
//              }

//  return $this->redirect(['view', 'id' => $model->id_akun]);
// }
           
//         else{

//  return $this->render('create', [
//             'model' => $model,

//         ]);
//         }

       
//        }
    


// public function actionCreate()
// {
// $model= new Akun();
// if($model->load(Yii::$app->request->post())) {
// $model->file=UploadedFile::getInstance($model,'file');
// $uploadExists=0;
// if($model->file){
// $imagepath='uploads/files/';
// $model->file_import=$imagepath .rand(10,100).'-'.str_replace('','-',$model->file->name);

// $bulkInsertArray = array();
// $uploadExists=1;
// }

// if($uploadExists){
// $model->file->saveAs($model->file_import);
// $handle=fopen($model->file_import,'r');
// if($handle){
// if($model->save()){
// #var_dump($model->errors);

// while(($line=fgetcsv($handle, 1000,",")) != FALSE) {
// $bulkInsertArray[]=[
// 'username'=>$line[0],
// 'password'=>$line[1],
// 'email'=>$line[2],
// 'status'=>$line[3],
// 'id_role'=>$line[4],
// ];
// }
// }
// fclose($handle);
// $tableName='akun';
// $columnNameArray=['username','password',
// 'email','status','id_role'];
// Yii::$app->db->createCommand()->akunInsert($tableName,$columnNameArray, $bulkInsertArray)->execute();
// #print_r($bulkInsertArray);
// }
// }
// return $this->redirect(['view','id'=>$model->id_akun]);
// }else{
// return $this->render('create',['model'=>$model,]);
// }
// }

 public function actionCreate()
    {
        $model = new Akun();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_akun]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Akun model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
         $role = \backend\models\Role::find()->all();
        $role = ArrayHelper::map($role,'id_role','role');


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_akun]);
        }

        return $this->render('update', [
            'model' => $model,
            'role'=> $role
        ]);
    }

    /**
     * Deletes an existing Akun model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionIndex2()
    {
        return $this->render('index1');
    }

    /**
     * Finds the Akun model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Akun the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Akun::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
