<?php

namespace backend\controllers;

use Yii;
use backend\models\Personalisasi;
use backend\models\PersonalisasiSearch;
use backend\models\Akun;
use backend\models\DetailPersonalisasi;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * PersonalisasiController implements the CRUD actions for Personalisasi model.
 */
class PersonalisasiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Personalisasi models.
     * @return mixed
     */
    public function actionIndex($status)
    {   
        $id = Yii::$app->user->id;
        $akun = Akun::findOne(['id_akun' => $id]);
        $searchModel = new PersonalisasiSearch();
        $dataProvider = new ActiveDataProvider([
            'query' => Personalisasi::find()->where(['id_akun' => $akun['id_akun']]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'status' => $status,
        ]);
    }

    /**
     * Displays a single Personalisasi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = Personalisasi::find()->where(['id_personalisasi'=>$id]);
        $dataProviderPersonalisasi = new \yii\data\ActiveDataProvider(['query'=>$model,]);

        $modelDetailPersonalisasi = DetailPersonalisasi::find()->where(['id_personalisasi'=>$id]);

        $dataProviderDetailPersonalisasi = new \yii\data\ActiveDataProvider([
                'query'=>$modelDetailPersonalisasi,
            ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProviderDetailPersonalisasi'=>$dataProviderDetailPersonalisasi,
        ]);
    }

    // /**
    //  * Displays a single Personalisasi model.
    //  * @param integer $id
    //  * @return mixed
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    // public function actionView2(){
    //     $idpersonalisasi=Yii::$app->user->id;;
    
    //     $model = new Personalisasi::find()->where(['id_personalisasi'=>$idpersonalisasi]);
    //     $models1DetailPersonalisasi = DetailPersonalisasi::find()->where(['id_personalisassi'=>$id]);
    //     $dataProviderDetailPersonalisasi = new \yii\data\ActiveDataProvider([
    //         'query'=>$models1DetailPersonalisasi ,
    //     ]);
    //     return $this->render('view', [
    //         'model' =>  $this->findModel($id),
    //                 'dataProviderDetailPersonalisasi'=>$dataProviderDetailPersonalisasi,

    // }

    /**
     * Creates a new Personalisasi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Personalisasi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_personalisasi]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Personalisasi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_personalisasi]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Personalisasi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Personalisasi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Personalisasi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Personalisasi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
