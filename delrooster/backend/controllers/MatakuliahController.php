<?php

namespace backend\controllers;

use Yii;
use backend\models\Matakuliah;
use backend\models\MatakuliahSearch;
use backend\models\Personalisasi;
use backend\models\DetailPersonalisasi;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * MatakuliahController implements the CRUD actions for Matakuliah model.
 */
class MatakuliahController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Matakuliah models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MatakuliahSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dosen = \backend\models\Dosen::find()->all();
        // $dosen = ArrayHelper::map($dosen,'id_dosen','nama_dosen');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'dosen' => $dosen,
        ]);
        isset($_REQUEST['selection']);
    }

    /**
     * Displays a single Matakuliah model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    

    /**
     * Creates a new Matakuliah model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $requestId = Yii::$app->request->post('selection');
        if($requestId){
            $model = new Personalisasi();
            // Yii::$app->db->createCommand('INSERT INTO personalisasi(id_akun) values ('.Yii::$app->user->id .')')->execute();
            $model->id_akun = Yii::$app->user->id;
            //date_default_timezone_set('Asia/Jakarta');
            $model->lastUpdate=date('Y-m-d H:i:s');
            $model->save();

            $personalisasi = Personalisasi::find()->where(["lastUpdate"=>$model->lastUpdate, "id_akun"=>$model->id_akun])->one();
            foreach ($requestId as $id_matakuliah) {
                $personalisasi_detail = new DetailPersonalisasi();
                $personalisasi_detail->id_personalisasi = $personalisasi->id_personalisasi;//Yii::$app->db->getLastInsertId();
                $personalisasi_detail->id_matakuliah = $id_matakuliah;
                if(!$personalisasi_detail->save()){
                 throw new Exception('Failed to save Detail Personalisasi');
                }
            }
            Yii::$app->getSession()->setFlash('success','Success');
        }else{
            Yii::$app->getSession()->setFlash('error','Error');
        }

        

    // return $this->render('create', [
    //         'model' => $model,]);
        return $this->redirect(['personalisasi/index','status' => 'view']);

    //     $model = new Matakuliah();
    //     $model2 = new Personalisasi();

    //     if ($model->load(Yii::$app->request->post('selection')) && $model->validate()) {
    //         try {
    //             $personalisasi = new Personalisasi();
    //             $personalisasi->akun = Yii::$app->user->id;
    //             if(!$personalisasi->save()) {
    //                 throw new Exception('Failed to save Loker');
    //             }
    //             foreach ($model->namamatakuliah as $id_matakuliah) {
    //                 $personalisasi_detail = new DetailPersonalisasi();
    //                 $personalisasi_detail->id_personalisasi = $personalisasi->id_personalisasi;
    //                 $personalisasi_detail->id_matakuliah = $id_matakuliah;

    //                 if(!$personalisasi_detail->save()){
    //                     throw new Exception('Failed to save Detail Personalisasi');
    //                 }
    //             }
    //         } catch (Exception $ex) {
    //             // Rollback if any save() failed
    //             $transaction->rollBack();
    //             die($ex->getMessage());
    //         }

    //         return $this->redirect(['view', 'id' => $personalisasi_detail->id_matakuliah  ]);
    //         } 
    //     else {
    //     date_default_timezone_set('Asia/Jakarta');
    //     $currentDate = date('d-m-Y');
    //     return $this->render(Url::to('personalisasi/index', [
    //     'model' => $model2,
    //     'create' => $currentDate,
    //     ]));
    
}

    /**
     * Updates an existing Matakuliah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model=new Personalisasi();
        $model = $this->findModel($id);
        $requestId = Yii::$app->request->post('selection');

        if ($requestId){
            date_default_timezone_set('Asia/Jakarta');
            $currentDate = date('Y-m-d H:i:s');
            $model->lastUpdate=$currentDate;
            $model->save();
            foreach ($requestId as $id_matakuliah) {
                if(!ArrayHelper::isIn($id_matakuliah)){
                    $oldpersonalisasi = DetailPersonalisasi::find()->where([
                        'id_matakuliah'=>$id_matakuliah,
                        'id_personalisasi'=>$id,
                        ])->one();
                    if(!$oldpersonalisasi->delete())
                            throw new Exception('Failed to delete old Personalisasi');
                }
            }
            foreach ($requestId as $id_matakuliah) {
                    if(!ArrayHelper::isIn($id_matakuliah)) {
                        $newTipes = new DetailPersonalisasi();
                        $newTipes->id_personalisasi = $id;
                        $newTipes->id_matakuliah = $id_matakuliah;

                        if(!$newTipes->save())
                            throw new Exception('Failed to save new Personalisasi');
                    }
                }

        }
        //return $this->redirect(['matakuliah/index','id' =>$model->id_personalisasi]);

        // return $this->render('matakuliah/index', [
        //     'model' => $model,
        //]);
    }

    /**
     * Deletes an existing Matakuliah model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

     // public function actionSimpan()
     //   {

     //   $selection=(array)Yii::$app->request->post('selection');//typecasting
     //   foreach($selection as $id){
     //        $e=Personalisasi::findOne((int)$id);//make a typecasting
     //        //do your stuff
     //        $e->save();
     //   }
     //     }

    /**
     * Finds the Matakuliah model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Matakuliah the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Matakuliah::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
