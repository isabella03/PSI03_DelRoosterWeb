<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Matakuliah;

/**
 * MatakuliahSearch represents the model behind the search form of `backend\models\Matakuliah`.
 */
class MatakuliahSearch extends Matakuliah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_matakuliah', 'sks'], 'integer'],
            [['kode_matakuliah', 'matakuliah', 'kategori', 'id_dosen'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Matakuliah::find();
        // $query->leftJoin('dosen', 'dosen.nama_dosen=matakuliah.id_dosen');
        $query->joinWith('dosen');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_matakuliah' => $this->id_matakuliah,
            'sks' => $this->sks,
            // 'dosen.nama_dosen' => $this->nama_dosen,
        ]);

        $query->andFilterWhere(['like', 'kode_matakuliah', $this->kode_matakuliah])
            ->andFilterWhere(['like', 'matakuliah', $this->matakuliah])
            ->andFilterWhere(['like', 'dosen.nama_dosen', $this->id_dosen]);
            //->andFilterWhere(['like', 'dosen.nama_dosen', $this-> id_dosen ]);
            //->andFilterWhere(['like', 'sesi', $this->sesi]);

        return $dataProvider;
    }
}
