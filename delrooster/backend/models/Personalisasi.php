<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "personalisasi".
 *
 * @property int $id_personalisasi
 * @property int $id_akun
 * @property string $lastUpdate
 *
 * @property DetailPersonalisasi[] $detailPersonalisasis
 * @property Jadwal[] $jadwals
 */
class Personalisasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personalisasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_akun'], 'integer'],
            [['lastUpdate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_personalisasi' => 'Id Personalisasi',
            'id_akun' => 'Id Akun',
            'lastUpdate' => 'Create',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailPersonalisasis()
    {
        return $this->hasMany(DetailPersonalisasi::className(), ['id_personalisasi' => 'id_personalisasi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::className(), ['id_personalisasi' => 'id_personalisasi']);
    }
}
