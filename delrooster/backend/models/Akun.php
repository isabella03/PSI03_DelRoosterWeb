<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "akun".
 *
 * @property int $id_akun
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $status
 * @property int $id_role
 * @property string file_import
 *
 * @property Role $role
 * @property Personalisasi[] $personalisasis
 */
class Akun extends \yii\db\ActiveRecord
{

    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akun';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_role'], 'integer'],
            [['username', 'password', 'email', 'status', 'file_import'], 'string', 'max' => 255],
            [['id_role'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['id_role' => 'id_role']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_akun' => 'Id Akun',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'status' => 'Status',
            'id_role' => 'Id Role',
            'file_import'=> 'File Import',
            'globalSearch'=>'Cari Akun'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id_role' => 'id_role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis()
    {
        return $this->hasMany(Personalisasi::className(), ['id_akun' => 'id_akun']);
    }
}
