<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "dosen".
 *
 * @property int $id_dosen
 * @property string $inisial_dosen
 * @property string $nama_dosen
 * @property string $email
 *
 * @property Jadwal[] $jadwals
 */
class Dosen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dosen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inisial_dosen', 'nama_dosen', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_dosen' => 'Id Dosen',
            'inisial_dosen' => 'Inisial Dosen',
            'nama_dosen' => 'Nama Dosen',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::className(), ['id_dosen' => 'id_dosen']);
    }
}
