<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jadwal".
 *
 * @property int $id_jadwal
 * @property string $sesi
 * @property string $hari
 * @property int $id_ruangan
 * @property int $id_kelas
 * @property int $id_matakuliah
 * @property int $id_dosen
 * @property int $id_personalisasi
 * @property string file_import
 * @property string $ruangan
  * @property string $kelas
   * @property string $dosen
    * @property string $singkatan_matakuliah
 *
 * @property Ruangan $ruangan
 * @property Kelas $kelas
 * @property TMatakuliah $matakuliah
 * @property Dosen $dosen
 * @property Personalisasi $personalisasi
 */
class Jadwal extends \yii\db\ActiveRecord
{

        public $file;
        
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jadwal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ruangan', 'id_kelas', 'id_matakuliah', 'id_dosen', 'id_personalisasi'], 'integer'],
            [['sesi'], 'string', 'max' => 4],
            // [['sesi'], 'string', 'max' => 4],
            [['hari','file_import','dosen','kelas','ruangan','singkatan_matakuliah'], 'string', 'max' => 255],
            [['id_ruangan'], 'exist', 'skipOnError' => true, 'targetClass' => Ruangan::className(), 'targetAttribute' => ['id_ruangan' => 'id_ruangan']],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id_kelas']],
            [['id_matakuliah'], 'exist', 'skipOnError' => true, 'targetClass' => Matakuliah::className(), 'targetAttribute' => ['id_matakuliah' => 'ID_Matakuliah']],
            [['id_dosen'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['id_dosen' => 'id_dosen']],
            [['id_personalisasi'], 'exist', 'skipOnError' => true, 'targetClass' => Personalisasi::className(), 'targetAttribute' => ['id_personalisasi' => 'id_personalisasi']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_jadwal' => 'Id Jadwal',
            'sesi' => 'Sesi',
            'hari' => 'Hari',
            'id_ruangan' => 'Ruangan Kelas',
            'id_kelas' => 'Id Kelas',
            'id_matakuliah' => 'Id Matakuliah',
            'id_dosen' => 'Id Dosen',
            'id_personalisasi' => 'Id Personalisasi',
            'file_import'=>'File Import',
            'globalSearch'=>'Cari',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuangan()
    {
        return $this->hasOne(Ruangan::className(), ['id_ruangan' => 'id_ruangan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatakuliah()
    {
        return $this->hasOne(Matakuliah::className(), ['id_matakuliah' => 'id_matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'id_dosen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasi()
    {
        return $this->hasOne(Personalisasi::className(), ['id_personalisasi' => 'id_personalisasi']);
    }
}
