<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sesi".
 *
 * @property int $id_sesi
 * @property string $sesi
 *
 * @property Matakuliah[] $matakuliahs
 */
class Sesi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sesi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sesi'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_sesi' => 'Id Sesi',
            'sesi' => 'Sesi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatakuliahs()
    {
        return $this->hasMany(Matakuliah::className(), ['id_sesi' => 'id_sesi']);
    }
}
