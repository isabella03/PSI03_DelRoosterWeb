<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Akun;

/**
 * AkunSearch represents the model behind the search form of `backend\models\Akun`.
 */
class AkunSearch extends Akun
{
    /**
     * @inheritdoc
     */

    public $globalSearch;
    public function rules()
    {
        return [
            [['id_akun'], 'integer'],
            [['username', 'id_role','password','file_import','globalSearch', 'email', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akun::find();
        $query->joinWith('role');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }





        // grid filtering conditions
        $query->andFilterWhere([
            'id_akun' => $this->id_akun,
            'id_role' => $this->id_role,
        ]);

        $query->orFilterWhere(['like', 'username', $this->globalSearch])
            ->orFilterWhere(['like', 'password', $this->globalSearch])
            ->orFilterWhere(['like', 'email', $this->globalSearch])
            ->orFilterWhere(['like', 'status', $this->globalSearch])
            ->orFilterWhere(['like','file_import', $this->globalSearch])
            ->orFilterWhere(['like','role.role',$this->globalSearch])
            ->orFilterWhere(['like','role.role',$this->id_role]);

        return $dataProvider;
    }
}
