<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ruangan".
 *
 * @property int $id_ruangan
 * @property int $nomor_ruangan
 * @property string $kategori
 *
 * @property Jadwal[] $jadwals
 */
class Ruangan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ruangan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor_ruangan'], 'integer'],
            [['kategori'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ruangan' => 'Id Ruangan',
            'nomor_ruangan' => 'Nomor Ruangan',
            'kategori' => 'Kategori',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::className(), ['id_ruangan' => 'id_ruangan']);
    }
}
