<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Jadwal;


/**
 * JadwalSearch represents the model behind the search form of `backend\models\Jadwal`.
 */
class JadwalSearch extends Jadwal
{
    /**
     * @inheritdoc
     */
public $globalSearch;

    public function rules()
    {
        return [
            [['id_jadwal'], 'integer'],
            [['sesi', 'globalSearch','id_personalisasi','id_dosen', 'id_matakuliah','id_kelas','id_ruangan','file_import', 'hari','dosen','kelas','singkatan_matakuliah','ruangan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Jadwal::find();
        // $query->joinWith(['ruangan']);
        // $query->joinWith(['kelas']);
        // $query->joinWith(['dosen']);
        // $query->joinWith(['matakuliah']);
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())){
            return $dataProvider;
        }

        // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'id_jadwal' => $this->id_jadwal,
        //     'id_ruangan' => $this->id_ruangan,
        //     'id_kelas' => $this->id_kelas,
        //     'id_matakuliah' => $this->id_matakuliah,
        //     'id_dosen' => $this->id_dosen,
        //     'id_personalisasi' => $this->id_personalisasi,
        // ]);

        $query->orFilterWhere(['like', 'sesi', $this->globalSearch])
            ->orFilterWhere(['like', 'hari', $this->globalSearch ])
            ->orFilterWhere(['like', 'ruangan', $this->globalSearch ])
            // ->orFilterWhere(['like', 'ruangan', $this->id_ruangan ])
            ->orFilterWhere(['like','kelas',$this->globalSearch])
            ->orFilterWhere(['like','dosen',$this->globalSearch])
            ->orFilterWhere(['like','singkatan_matakuliah',$this->globalSearch]);
            // ->orFilterWhere(['like','kelas.kelas',$this->globalSearch]);
            // ->orFilterWhere(['like', 'nomor_ruangan', $this->globalSearch ]);
            // ->orFilterWhere(['like', 'ruangan.nomor_ruangan', $this->globalSearch ]);

        return $dataProvider;
    }
}
