<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "detail_personalisasi".
 *
 * @property int $detail_personalisasi_id
 * @property int $id_personalisasi
 * @property int $id_matakuliah
 *
 * @property Personalisasi $personalisasi
 * @property Matakuliah $matakuliah
 */
class DetailPersonalisasi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'detail_personalisasi';
    }

    public $matakuliah;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_personalisasi', 'id_matakuliah'], 'integer'],
            [['id_personalisasi'], 'exist', 'skipOnError' => true, 'targetClass' => Personalisasi::className(), 'targetAttribute' => ['id_personalisasi' => 'id_personalisasi']],
            [['id_matakuliah'], 'exist', 'skipOnError' => true, 'targetClass' => Matakuliah::className(), 'targetAttribute' => ['id_matakuliah' => 'id_matakuliah']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'detail_personalisasi_id' => 'Detail Personalisasi ID',
            'id_personalisasi' => 'Id Personalisasi',
            'id_matakuliah' => 'Id Matakuliah',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasi()
    {
        return $this->hasOne(Personalisasi::className(), ['id_personalisasi' => 'id_personalisasi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getidMatakuliah()
    {
        return $this->hasOne(Matakuliah::className(), ['id_matakuliah' => 'id_matakuliah']);
    }
}
