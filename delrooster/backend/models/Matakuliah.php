<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "matakuliah".
 *
 * @property int $id_matakuliah
 * @property string $kode_matakuliah
 * @property string $matakuliah
 * @property int $sks
 * @property string $kategori
 * @property int $id_dosen
 * @property int $id_kelas
 * @property string $singkatan
 * @property int $id_sesi
 *
 * @property Jadwal[] $jadwals
 * @property Dosen $dosen
 * @property Kelas $kelas
 * @property Sesi $sesi0
 * @property Personalisasi[] $personalisasis
 * @property Personalisasi[] $personalisasis0
 * @property Personalisasi[] $personalisasis1
 * @property Personalisasi[] $personalisasis2
 * @property Personalisasi[] $personalisasis3
 * @property Personalisasi[] $personalisasis4
 * @property Personalisasi[] $personalisasis5
 * @property Personalisasi[] $personalisasis6
 * @property Personalisasi[] $personalisasis7
 */
class Matakuliah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'matakuliah';
    }
    public $namamatakuliah;
    public $isNewRecord = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_matakuliah', 'matakuliah', 'sks', 'kategori'], 'required'],
            [['sks', 'id_dosen', 'id_kelas', 'id_sesi'], 'integer'],
            [['kode_matakuliah', 'matakuliah', 'singkatan'], 'string', 'max' => 255],
            [['kategori'], 'string', 'max' => 4],
            [['id_dosen'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['id_dosen' => 'id_dosen']],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::className(), 'targetAttribute' => ['id_kelas' => 'id_kelas']],
            [['id_sesi'], 'exist', 'skipOnError' => true, 'targetClass' => Sesi::className(), 'targetAttribute' => ['id_sesi' => 'id_sesi']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_matakuliah' => 'Id Matakuliah',
            'kode_matakuliah' => 'Kode Matakuliah',
            'matakuliah' => 'Matakuliah',
            'sks' => 'Sks',
            'kategori' => 'Sesi',
            'id_dosen' => 'Dosen',
            'id_kelas' => 'Id Kelas',
            'singkatan' => 'Singkatan',
            'id_sesi' => 'Id Sesi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJadwals()
    {
        return $this->hasMany(Jadwal::className(), ['id_matakuliah' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'id_dosen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::className(), ['id_kelas' => 'id_kelas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSesi()
    {
        return $this->hasOne(Sesi::className(), ['id_sesi' => 'id_sesi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah10' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis0()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah1' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis1()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah2' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis2()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah3' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis3()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah4' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis4()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah5' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis5()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah6' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis6()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah7' => 'ID_Matakuliah']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalisasis7()
    {
        return $this->hasMany(Personalisasi::className(), ['id_matakuliah9' => 'ID_Matakuliah']);
    }

    /**
     * Get List of Matakuliah List
     */
    public static function getMatakuliahList()
    {
        $droptions = Matakuliah::find()->asArray()->all();

        return ArrayHelper::map($droptions, 'id_matakuliah', 'matakuliah');
    }

}
