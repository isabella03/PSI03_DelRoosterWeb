/*
SQLyog Community v12.2.1 (64 bit)
MySQL - 10.1.31-MariaDB : Database - del2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`del2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `del2`;

/*Table structure for table `akun` */

DROP TABLE IF EXISTS `akun`;

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  `file_import` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_akun`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `akun_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `akun` */

insert  into `akun`(`id_akun`,`username`,`password`,`email`,`status`,`id_role`,`file_import`) values 
(1,'desy','$2y$13$KecMZTtTPyOeBav2hjiLOO8JUUfMuzEt9GVw/YL8.wCXnpotL8FO.','desy@gmail.com','Mahasiswa',1,NULL),
(2,'isabella','$2y$13$KecMZTtTPyOeBav2hjiLOO8JUUfMuzEt9GVw/YL8.wCXnpotL8FO.','iss15011@del.ac.id','Mahasiswa Tidak Aktif',3,NULL),
(6,'intriwanty','$2y$13$zeiXJQculsGRNqerUCw6vuzrMWj0IUie21KoiwntcpQ7Cv7jhsjgK','desy@del.ac.id','Mahasiswa',2,NULL),
(7,'ariansyahn','$2y$13$KecMZTtTPyOeBav2hjiLOO8JUUfMuzEt9GVw/YL8.wCXnpotL8FO.','ariansyahn@del.ac.id','Mahasiswa',1,NULL),
(15,'Aldoni Hutapea','aldo','iss15008@del.ac.id','Mahasiswa Aktif',3,NULL),
(17,'Isabella Siagian','isabella','iss15011@del.ac.id','Mahasiswa Aktif',NULL,NULL),
(18,'Safiah Sitorus','safiah','iss15012@del.ac.id','Mahasiswa Aktif',NULL,NULL),
(19,'Melani Tambun','melani','iss15013@del.ac.id','Mahasiswa Aktif',NULL,NULL);

/*Table structure for table `detail_personalisasi` */

DROP TABLE IF EXISTS `detail_personalisasi`;

CREATE TABLE `detail_personalisasi` (
  `detail_personalisasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_personalisasi` int(11) DEFAULT NULL,
  `id_matakuliah` int(11) DEFAULT NULL,
  PRIMARY KEY (`detail_personalisasi_id`),
  KEY `id_personalisasi` (`id_personalisasi`),
  KEY `id_matakuliah` (`id_matakuliah`),
  CONSTRAINT `detail_personalisasi_ibfk_1` FOREIGN KEY (`id_personalisasi`) REFERENCES `personalisasi` (`id_personalisasi`),
  CONSTRAINT `detail_personalisasi_ibfk_2` FOREIGN KEY (`id_matakuliah`) REFERENCES `matakuliah` (`id_matakuliah`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `detail_personalisasi` */

insert  into `detail_personalisasi`(`detail_personalisasi_id`,`id_personalisasi`,`id_matakuliah`) values 
(1,1,1),
(2,1,2),
(3,1,3),
(25,37,10),
(26,37,11),
(27,38,1),
(28,38,2),
(29,39,2),
(30,39,3),
(31,39,4),
(32,39,5),
(33,39,6),
(34,39,7),
(35,40,62),
(36,41,1),
(37,42,5),
(38,42,29),
(39,42,80),
(40,42,81),
(41,43,55),
(42,44,3);

/*Table structure for table `dosen` */

DROP TABLE IF EXISTS `dosen`;

CREATE TABLE `dosen` (
  `id_dosen` int(11) NOT NULL AUTO_INCREMENT,
  `inisial_dosen` varchar(255) DEFAULT NULL,
  `nama_dosen` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_dosen`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

/*Data for the table `dosen` */

insert  into `dosen`(`id_dosen`,`inisial_dosen`,`nama_dosen`,`email`) values 
(1,'ADL','Adelina Manurung, S.Si, M.Sc','adelina.manurung@del.ac.id'),
(2,'','Andy Trirakhmadi, S.T, M.T',''),
(3,'ART','Anthon Roberto Tampubolon, S.Kom, M.T',''),
(4,'ASD','Arie Satia Dharma, S.T, M.Kom',''),
(5,'BLT','Bonar Lumban Tobing',''),
(6,'','Charla Tri Selda Manik, ST.,M.Eng',''),
(7,'','Christin Erniati Panjaitan, ST., M.Sc',''),
(8,'','Christoper Janwar Saputra Sinaga',''),
(9,'','Devis Wawan Saputra, ST., MBA',''),
(10,'ACB','Dr. Arlinta Christy Barus, S.T., M.InfoTach',''),
(11,'','Dr. Arnaldo Marulitua Sinaga, S.T., M.InfoTech',''),
(12,'','Dr. Inggriani Liem',''),
(13,'','Dr. Ir. Bambang S.P. Abednego',''),
(14,'','Dr. Merry Meryam Martgrita, S.Si, M.Si',''),
(15,'','Dr. Sony Adhi Susanto, S.Si, M.Sc',''),
(16,'','Dra. Roga Florida Kembaren, M.Si',''),
(17,'','Eka Stephani Sinambela, SST., M.Sc',''),
(18,'','Eka Trisno Samosir, S.Kom, PGDip',''),
(19,'','Ellyas Alga Nainggolan, S.TP, M.Sc',''),
(20,'','Ernie Bertha Nababan, S.Pd, M.Pd',''),
(21,'','Fidelis Haposan Silalahi, SH., MH',''),
(22,'','Fitriani Tupa Ronauli Silalahi, S.Si, M.Si',''),
(23,'','Gerry Italiano Wowiling, S.Tr.Kom',''),
(24,'','Good Friend Panggabean, ST, MT',''),
(25,'','Hadi Sutanto Saragi, S.T, M.Eng',''),
(26,'','Hj. Latifah Sudarmy, S.Ag.',''),
(27,'GDE','I Gde Eka Dirgayussa, S.Pd, M.Si',''),
(28,'IFY','Ike Fitriyaningsih, S.Si., M.Si',''),
(29,'','Indra Hartanto Tambunan, Ph.D.',''),
(30,'','Inte Christinawati Bu\'ulolo, ST., M.T.I',''),
(31,'','Ir. Partumpuhan Naiborhu, M.M',''),
(32,'','Kisno, S.Pd, M.Pd',''),
(33,'LMG','Lit Malem Ginting, S.Si. MT',''),
(34,'YMA','Dr. Yosef Barita Sar Manik, S.T, M.Sc',''),
(35,'','M. Yusuf Hakim Widianto, M.Si. M.Sc',''),
(36,'MVL','Maisevli Harika, M.T, M.Eng',''),
(37,'ANA','Mariana Simanjuntak, S.S, M.Sc',''),
(38,'MSS','Mario Elyezer Subekti Simaremare, S.Kom., M.Sc',''),
(39,'','Marojahan Mula Timbul Sigiro, ST, M.Sc',''),
(40,'','Monita Pasaribu, S.Si., MT.',''),
(41,'MSL','Mukhammad Solikhin, S.Si, M.Si',''),
(42,'','Nenni Mona Aruan, S.Pd., M.Si',''),
(43,'','Niko Saripson P. Simamora',''),
(44,'','Olnes Yosefa Hutajulu, S.Pd., M.Eng',''),
(45,'','Pandapotan Napitupulu, ST, M.T',''),
(46,'','Pandapotan Siagian, ST, M.Eng',''),
(47,'PAT','Parmonangan Rotua Togatorop, S.Kom., M.T.I',''),
(48,'','Prof. Dr.Roberd Saragih, MT',''),
(49,'','Prof. Ir. Togar M. Simatupang, M.Tech., Ph.D',''),
(50,'RMS','Rentauli Mariah Silalahi, S.Pd, M.Ed',''),
(51,'RCS','Ricardo Chandra Situmeang, S.Psi, M.A',''),
(52,'','Rien Rakhmana, S.Si, M.T',''),
(53,'','Rismal, S.Pd., M.Si',''),
(54,'','Riyanthi Angrainy Sianturi, S.Sos, M.Ds',''),
(55,'RZS','Rizal Horas Manahan Sinaga, S.Si., M.T',''),
(56,'','Roy Deddy Hasiholan Lumban Tobing, S.T., M.ICT',''),
(57,'RMM','Rumondang Miranda Marsaulina, S.P, M.Si',''),
(58,'SGS','Samuel Indra Gunawan Situmeang',''),
(59,'SAM','Santi Agustina Manalu, S.S., M.Pd',''),
(60,'','Santi Febri Arianti, S.Si, M.Sc',''),
(61,'','Saswinadi Sasmojo',''),
(62,'','Teamsar Muliadi Panggabean',''),
(63,'THS','Tennov Simanjuntak, S.T, M.Sc',''),
(64,'','Tiurma Lumban Gaol, SP, M.P',''),
(65,'','Togu Novriansyah Turnip, S.S.T., M.IM',''),
(66,'','Tulus Pardamean Simanjuntak. SST',''),
(67,'','Verawaty Situmorang, S.Kom., M.T.I',''),
(68,'','Wiwin Sry Adinda Banjarnahor, S.Kom, M.Sc',''),
(69,'YYS','Yaya Setiyadi, S.Si, M.T',''),
(70,'','Yohannes Pratama, S.Si. M.T',''),
(71,'','Yulisa Lestari., S.Si, M.T',''),
(72,'YBS','Yuniarta Basni, S.Si. M.Si','');

/*Table structure for table `jadwal` */

DROP TABLE IF EXISTS `jadwal`;

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `sesi` char(4) DEFAULT NULL,
  `hari` varchar(255) DEFAULT NULL,
  `id_ruangan` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_matakuliah` int(11) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `id_personalisasi` int(11) DEFAULT NULL,
  `file_import` varchar(255) DEFAULT NULL,
  `singkatan_matakuliah` varchar(255) DEFAULT NULL,
  `dosen` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `ruangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal`),
  KEY `id_ruangan` (`id_ruangan`),
  KEY `id_kelas` (`id_kelas`),
  KEY `id_matakuliah` (`id_matakuliah`),
  KEY `id_dosen` (`id_dosen`),
  KEY `id_personalisasi` (`id_personalisasi`),
  CONSTRAINT `jadwal_ibfk_1` FOREIGN KEY (`id_ruangan`) REFERENCES `ruangan` (`id_ruangan`),
  CONSTRAINT `jadwal_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  CONSTRAINT `jadwal_ibfk_3` FOREIGN KEY (`id_matakuliah`) REFERENCES `matakuliah` (`id_matakuliah`),
  CONSTRAINT `jadwal_ibfk_4` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id_dosen`),
  CONSTRAINT `jadwal_ibfk_5` FOREIGN KEY (`id_personalisasi`) REFERENCES `personalisasi` (`id_personalisasi`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1;

/*Data for the table `jadwal` */

insert  into `jadwal`(`id_jadwal`,`sesi`,`hari`,`id_ruangan`,`id_kelas`,`id_matakuliah`,`id_dosen`,`id_personalisasi`,`file_import`,`singkatan_matakuliah`,`dosen`,`kelas`,`ruangan`) values 
(6,'2','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'DEL CHA','ANA','11SI2','AUD'),
(7,'3','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'BASDAT','AHF','12SI1','GD933'),
(8,'3','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'BASDAT','AHF','12SI2','GD933'),
(19,'1','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'FISDAS I','NMA','11SI1','GD938'),
(20,'1','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'FISDAS I','NMA','11SI2','GD938'),
(21,'2','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'FISDAS I','NMA','11SI1','GD938'),
(35,'1','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'MADAS I','MSL','11SI1','GD925'),
(36,'1','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'MADAS I','MSL','11SI2','GD925'),
(37,'2','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'MADAS I','MSL','11SI1','GD925'),
(51,'1','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'BAMI','AHF','14SI1','GD933'),
(52,'1','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'BAMI','AHF','14SI2','GD933'),
(53,'2','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'BAMI','AHF','14SI1','GD933'),
(67,'1','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'PABWE','MSS','13SI1','GD933'),
(68,'1','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'PABWE','MSS','13SI2','GD933'),
(69,'2','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'PABWE','MSS','13SI1','GD933'),
(132,'4','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'BASDAT','AHF','12SI1','GD933'),
(133,'4','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'BASDAT','AHF','12SI2','GD933'),
(134,'5','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'CERTAN','AHF','13SI1','GD934'),
(135,'5','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'CERTAN','AHF','13SI2','GD934'),
(136,'6','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'CERTAN','AHF','13SI1','GD934'),
(137,'6','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'CERTAN','AHF','13SI2','GD934'),
(138,'7','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'AUDTI','MSS','14SI1','GD935'),
(139,'7','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'AUDTI','MSS','14SI2','GD935'),
(140,'8','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'AUDTI','MSS','14SI1','GD935'),
(141,'8','Senin',NULL,NULL,NULL,NULL,NULL,NULL,'AUDTI','MSS','14SI2','GD935'),
(142,'2','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'FISDAS I','NMA','11SI2','GD938'),
(143,'3','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'DSI','THS','11SI1','GD935'),
(144,'3','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'DSI','THS','11SI2','GD935'),
(145,'4','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'DSI','THS','11SI1','GD935'),
(146,'4','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'DSI','THS','11SI2','GD935'),
(147,'5','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'MATDIS','RML','12SI1','GD933'),
(148,'5','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'MATDIS','RML','12SI2','GD933'),
(149,'6','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'MATDIS','RML','12SI1','GD933'),
(150,'6','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'MATDIS','RML','12SI2','GD933'),
(151,'7','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'ALSRUDAT','THS','12SI1','GD935'),
(152,'7','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'ALSRUDAT','THS','12SI2','GD935'),
(153,'8','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'ALSRUDAT','THS','12SI1','GD935'),
(154,'8','Selasa',NULL,NULL,NULL,NULL,NULL,NULL,'ALSRUDAT','THS','12SI2','GD935'),
(155,'2','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'MADAS I','MSL','11SI2','GD925'),
(156,'3','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'PROBSTAT','IFY','12SI1','GD933'),
(157,'3','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'PROBSTAT','IFY','12SI2','GD933'),
(158,'4','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'PROBSTAT','IFY','12SI1','GD933'),
(159,'4','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'PROBSTAT','IFY','12SI2','GD933'),
(160,'5','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'CERTAN','AHF','13SI1','GD925'),
(161,'5','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'CERTAN','AHF','13SI2','GD925'),
(162,'6','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'CERTAN','AHF','13SI1','GD925'),
(163,'6','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'CERTAN','AHF','13SI2','GD925'),
(164,'7','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'TA 1','THS','14SI1','GD925'),
(165,'7','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'TA 1','THS','14SI2','GD925'),
(166,'8','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'TA 1','THS','14SI1','GD925'),
(167,'8','Rabu',NULL,NULL,NULL,NULL,NULL,NULL,'TA 1','THS','14SI2','GD925'),
(168,'2','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'BAMI','AHF','14SI2','GD933'),
(169,'3','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'ALSRUDAT','THS','12SI1','GD935'),
(170,'3','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'ALSRUDAT','THS','12SI2','GD935'),
(171,'4','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'ALSRUDAT','THS','12SI1','GD935'),
(172,'4','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'ALSRUDAT','THS','12SI2','GD935'),
(173,'5','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'BASDATLAN','THS','13SI1','GD934'),
(174,'5','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'BASDATLAN','THS','13SI2','GD934'),
(175,'6','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'BASDATLAN','THS','13SI1','GD934'),
(176,'6','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'BASDATLAN','THS','13SI2','GD934'),
(177,'7','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'DAMI','SGS','14SI1','GD925'),
(178,'7','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'DAMI','SGS','14SI2','GD925'),
(179,'8','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'DAMI','SGS','14SI1','GD925'),
(180,'8','Kamis',NULL,NULL,NULL,NULL,NULL,NULL,'DAMI','SGS','14SI2','GD925'),
(181,'2','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'PABWE','MSS','13SI2','GD933'),
(182,'3','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'BASDAT','AHF','12SI1','GD935'),
(183,'3','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'BASDAT','AHF','12SI2','GD935'),
(184,'4','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'BASDAT','AHF','12SI1','GD935'),
(185,'4','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'BASDAT','AHF','12SI2','GD935'),
(186,'5','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'AUDTI','MSS','14SI1','GD933'),
(187,'5','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'AUDTI','MSS','14SI2','GD933'),
(188,'6','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'AUDTI','MSS','14SI1','GD933'),
(189,'6','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'AUDTI','MSS','14SI2','GD933'),
(190,'7','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'PROBSTAT','IFY','12SI1','GD925'),
(191,'7','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'PROBSTAT','IFY','12SI2','GD925'),
(192,'8','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'PROBSTAT','IFY','12SI1','GD925'),
(193,'8','Jumat',NULL,NULL,NULL,NULL,NULL,NULL,'PROBSTAT','IFY','12SI2','GD925');

/*Table structure for table `kelas` */

DROP TABLE IF EXISTS `kelas`;

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(255) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `kelas` */

insert  into `kelas`(`id_kelas`,`kelas`,`prodi`) values 
(1,'31TI1','D3 Teknik Informatika'),
(2,'31TI2','D3 Teknik Informatika'),
(3,'31TK','D3 Teknik Komputer'),
(4,'41TI','D4 Teknik Informatika'),
(5,'11TI1','S1 Teknik Informatika'),
(6,'11TI2','S1 Teknik Informatika'),
(7,'11SI1','S1 Sistem Informasi'),
(8,'11SI2','S1 Sistem Informasi'),
(9,'11MR1','S1 Manajemen Rekayasa'),
(10,'11MR2','S1 Manajemen Rekayasa'),
(11,'11TB','S1 Teknik Bioproses'),
(12,'32TI1','D3 Teknik Informatika'),
(13,'32TI2','D3 Teknik Informatika'),
(14,'32TK','D3 Teknik Komputer'),
(15,'42TI','D4 Teknik Informatika'),
(16,'12TI1','S1 Teknik Informatika'),
(17,'12TI2','S1 Teknik Informatika'),
(18,'12SI1','S1 Sistem Informasi'),
(19,'12SI2','S1 Sistem Informasi'),
(20,'12MR1','S1 Manajemen Rekayasa'),
(21,'12MR2','S1 Manajemen Rekayasa'),
(22,'12TB','S1 Teknik Bioproses'),
(23,'11TE1','S1 Teknik Elektro'),
(24,'11TE2','S1 Teknik Elektro'),
(25,'12TE1','S1 Teknik Elektro'),
(26,'12TE2','S1 Teknik Elektro'),
(27,'33TI','D3 Teknik Informatika'),
(28,'33TK','D3 Teknik Komputer'),
(29,'43TI','D4 Teknik Informatika'),
(30,'13TI1','S1 Teknik Informatika'),
(31,'13TI2','S1 Teknik Informatika'),
(32,'13SI1','S1 Sistem Informasi'),
(33,'13SI2','S1 Sistem Informasi'),
(34,'13TE1','S1 Teknik Elektro'),
(35,'13TE2','S1 Teknik Elektro'),
(36,'13MR1','S1 Manajemen Rekayasa'),
(37,'13MR2','S1 Manajemen Rekayasa'),
(38,'44TI','D4 Teknik Informatika'),
(39,'14TI1','S1 Teknik Informatika'),
(40,'14TI2','S1 Teknik Informatika'),
(41,'14SI1','S1 Sistem Informasi'),
(42,'14SI2','S1 Sistem Informasi'),
(43,'14TE','S1 Teknik Elektro'),
(44,'14MR','S1 Manajemen Rekayasa'),
(45,'14TB','S1 Teknik Bioproses'),
(46,'13TB','');

/*Table structure for table `matakuliah` */

DROP TABLE IF EXISTS `matakuliah`;

CREATE TABLE `matakuliah` (
  `id_matakuliah` int(11) NOT NULL AUTO_INCREMENT,
  `kode_matakuliah` varchar(255) NOT NULL,
  `matakuliah` varchar(255) NOT NULL,
  `sks` int(11) NOT NULL,
  `kategori` char(4) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `singkatan` varchar(255) DEFAULT NULL,
  `id_sesi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_matakuliah`),
  KEY `id_dosen` (`id_dosen`),
  KEY `id_kelas` (`id_kelas`),
  KEY `id_sesi` (`id_sesi`),
  CONSTRAINT `matakuliah_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id_dosen`),
  CONSTRAINT `matakuliah_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  CONSTRAINT `matakuliah_ibfk_3` FOREIGN KEY (`id_sesi`) REFERENCES `sesi` (`id_sesi`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

/*Data for the table `matakuliah` */

insert  into `matakuliah`(`id_matakuliah`,`kode_matakuliah`,`matakuliah`,`sks`,`kategori`,`id_dosen`,`id_kelas`,`singkatan`,`id_sesi`) values 
(1,'ISS1101','Sains Teknologi dan Seni di Masyarakat',2,'T',14,NULL,'STA',NULL),
(2,'MAS1101','Matematika Dasar I',4,'T',41,NULL,'MADAS I',NULL),
(3,'FIS1101','Fisika Dasar I',4,'T',42,NULL,'FISDAS I',NULL),
(4,'KUS1102','Bahasa Inggris I',2,'T',50,NULL,'ENG I',NULL),
(5,'KUS1101','Pembentukan Karakter Del',2,'T',37,NULL,'DEL CHA',NULL),
(6,'IFS1101','Pengantar Teknologi Informasi',2,'T',4,NULL,'PTI',NULL),
(7,'ISS1102','Pemrograman I',2,'T',11,NULL,'PROG I',NULL),
(8,'FIS1101','Fisika Dasar I (P)',4,'P',42,NULL,'FISDAS I',NULL),
(9,'IFS1101','Pengantar Teknologi Informasi (P)',2,'P',4,NULL,'PTI',NULL),
(10,'ISS1102','Pemrograman I (P)',2,'P',11,NULL,'PROG I',NULL),
(11,'MAS1201','Matematika Dasar II',4,'T',72,NULL,'MADAS II',NULL),
(12,'FIS1201','Fisika Dasar II',4,'T',42,NULL,'FISDAS II',NULL),
(13,'FIS1201','Fisika Dasar II (P)',4,'P',42,NULL,'FISDAS II',NULL),
(14,'KUS1201','Bahasa Inggris II',2,'T',50,NULL,'ENG II',NULL),
(15,'ISS1001','Dasar Sistem Informasi',3,'T',63,NULL,'DSI',NULL),
(16,'ISS1001','Dasar Sistem Informasi (P)',3,'P',63,NULL,'DSI',NULL),
(17,'ISS1201','Pengantar Desain Rekayasa',2,'T',34,NULL,'PDR',NULL),
(18,'ISS1202','Pemrograman II',2,'T',38,NULL,'PROG II-IS',NULL),
(19,'ISS1202','Pemrograman II (P)',2,'P',38,NULL,'PROG II-IS',NULL),
(20,'KUS1011','Tata Tulis Karya Ilmiah',2,'T',64,NULL,'TTKI',NULL),
(21,'MAS2102','Matematika Diskrit',3,'T',53,NULL,'MATDIS',NULL),
(22,'MAS2001','Probabilitas dan Statistika',3,'T',28,NULL,'PROBSTAT',NULL),
(23,'IFS2101','Algoritma dan Struktur Data',3,'T',63,NULL,'ALSRUDAT',NULL),
(24,'IFS2101','Algoritma dan Struktur Data (P)',3,'P',63,NULL,'ALSRUDAT',NULL),
(25,'ELS2108','Sistem Digital',3,'T',27,NULL,'SISDIG',NULL),
(26,'ELS2108','Sistem Digital (P)',3,'P',27,NULL,'SISDIG',NULL),
(27,'ISS2101','Basis Data',3,'T',47,NULL,'BASDAT',NULL),
(28,'ISS2101','Basis Data (P)',3,'P',47,NULL,'BASDAT',NULL),
(29,'ISS2102','Organisasi dan Manajemen Industri',3,'T',37,NULL,'OMI',NULL),
(30,'NWS2202','Sistem Operasi',3,'T',63,NULL,'SISOP',NULL),
(31,'NWS2202','Sistem Operasi (P)',3,'P',63,NULL,'SISOP',NULL),
(32,'ISS2201','Analisis Kebutuhan Sistem',3,'T',47,NULL,'AKS',NULL),
(33,'ISS2201','Analisis Kebutuhan Sistem (P)',3,'P',47,NULL,'AKS',NULL),
(34,'NWS2201','Arsitektur dan Organisasi Komputer',3,'T',17,NULL,'AOK',NULL),
(35,'NWS2201','Arsitektur dan Organisasi Komputer (P)',3,'P',17,NULL,'AOK',NULL),
(36,'IFS2203','Pemrograman Berorientasi Objek',3,'T',38,NULL,'PBO',NULL),
(37,'IFS2203','Pemrograman Berorientasi Objek (P)',3,'P',38,NULL,'PBO',NULL),
(38,'IFS1202','Dasar Rekayasa Perangkat Lunak',3,'T',4,NULL,'DRPL',NULL),
(39,'IFS1202','Dasar Rekayasa Perangkat Lunak (P)',3,'P',4,NULL,'DRPL',NULL),
(40,'IFS2201','Interaksi Manusia dan Komputer',3,'T',54,NULL,'IMK',NULL),
(41,'IFS2201','Interaksi Manusia dan Komputer (P)',3,'P',54,NULL,'IMK',NULL),
(42,'MRS3180','Technopreneurship',2,'T',9,NULL,'TECHNO',NULL),
(43,'MRS3180','Technopreneurship (P)',2,'P',9,NULL,'TECHNO',NULL),
(44,'ISS3101','Basis Data Lanjut',3,'T',63,NULL,'BASDATLAN',NULL),
(45,'ISS3101','Basis Data Lanjut (P)',3,'P',63,NULL,'BASDATLAN',NULL),
(46,'ISS3102 ','Kecerdasan Buatan',4,'T',58,NULL,'CERTAN',NULL),
(47,'ISS3102 ','Kecerdasan Buatan (P)',4,'P',58,NULL,'CERTAN',NULL),
(48,'ISS3103 ','Pemrograman Aplikasi Berbasis Web',4,'T',38,NULL,'PABWE',NULL),
(49,'ISS3103 ','Pemrograman Aplikasi Berbasis Web (P)',4,'P',38,NULL,'PABWE',NULL),
(50,'ISS3104 ','	Pengantar Jaringan Komputer',3,'T',39,NULL,'PJK',NULL),
(51,'ISS3104 ','	Pengantar Jaringan Komputer (P)',3,'P',39,NULL,'PJK',NULL),
(52,'KUS2001','Bahasa Inggris III',2,'T',50,NULL,'ENG III',NULL),
(53,'ISS3202','	Keamanan Sistem',3,'T',63,NULL,'KESIS',NULL),
(54,'ISS3202','Keamanan Sistem (P)',3,'P',63,NULL,'KESIS',NULL),
(55,'ISS3201','	Proyek Sistem Informasi',4,'T',38,NULL,'PRO-SI',NULL),
(56,'ISS3201','	Proyek Sistem Informasi (P)',4,'P',38,NULL,'PRO-SI',NULL),
(57,'ISS3203','	Gudang Data dan Kecerdasan Bisnis',3,'T',47,NULL,'DBI',NULL),
(58,'ISS3203','	Gudang Data dan Kecerdasan Bisnis (P)',3,'P',47,NULL,'DBI',NULL),
(59,'ISS3204','Socio-Informatika dan Profesionalisme',2,'T',10,NULL,'SOSPRO',NULL),
(61,'ISS3205','	Perencanaan Sumber Daya Perusahaan',4,'T',47,NULL,'ERP',NULL),
(62,'ISS3205','	Perencanaan Sumber Daya Perusahaan (P)',4,'P',47,NULL,'ERP',NULL),
(63,'BPS3280','Analisis Dampak Lingkungan',2,'T',71,NULL,'ADL',NULL),
(64,'KUS2201','	Agama dan Etika',2,'T',5,NULL,'ATI',NULL),
(65,'ISS4190','Kerja Praktek',2,'T',11,NULL,'KP',NULL),
(66,'ISS4191','Tugas Akhir 1 dan Seminar',4,'T',63,NULL,'TA 1',NULL),
(67,'ISS4101','Topik Khusus Bidang Minat Sistem Enterprise',2,'T',58,NULL,'SISENT',NULL),
(68,'KUS4101','Pancasila dan Kewarganegaraan',2,'T',31,NULL,'PC',NULL),
(69,'ISS4008','Audit Teknologi Informasi',3,'T',38,NULL,'AUDTI',NULL),
(70,'ISS4014','Pemrosesan Teks dan Bahasa Alami',3,'T',38,NULL,'BAMI',NULL),
(71,'ISS4003','Data Mining',3,'T',58,NULL,'DAMI',NULL),
(72,'ISS4015','Keamanan Sistem Lanjut',3,'T',63,NULL,'KESLAN',NULL),
(73,'KUS3201','	Hukum dan Etika Cyber',2,'T',21,NULL,'HEBER',NULL),
(74,'ISS4290','	Tugas Akhir 2',4,'T',47,NULL,'TA II',NULL),
(75,'ISS4018','	Manajemen Proyek',3,'T',47,NULL,'MANPRO',NULL),
(76,'ISS4011','Sistem Temu Balik Informasi',3,'T',58,NULL,'INRE',NULL),
(77,'IFS4021','Rekayasa Perangkat Lunak Berbasis Komponen',3,'T',33,NULL,NULL,NULL),
(78,'IFS4020','	Pembangunan Aplikasi Berbasis Service',3,'T',4,NULL,NULL,NULL),
(79,'IFS4027','Pembelajaran Mesin Lanjut',3,'T',62,NULL,NULL,NULL),
(80,'MRS4281','Kepemimpinan Bisnis',3,'T',37,NULL,NULL,NULL),
(81,'MRS4201','	Etika Profesional',2,'T',37,NULL,NULL,NULL);

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values 
('m000000_000000_base',1523942842),
('m130524_201442_init',1523942846);

/*Table structure for table `personalisasi` */

DROP TABLE IF EXISTS `personalisasi`;

CREATE TABLE `personalisasi` (
  `id_personalisasi` int(11) NOT NULL AUTO_INCREMENT,
  `id_akun` int(11) DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id_personalisasi`),
  KEY `id_akun` (`id_akun`),
  CONSTRAINT `personalisasi_ibfk_1` FOREIGN KEY (`id_akun`) REFERENCES `akun` (`id_akun`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `personalisasi` */

insert  into `personalisasi`(`id_personalisasi`,`id_akun`,`lastUpdate`) values 
(1,7,'2018-06-04 20:42:37'),
(2,1,'2018-06-04 20:42:50'),
(37,1,'2018-06-06 16:11:02'),
(38,1,'2018-06-06 11:14:01'),
(39,7,'2018-06-06 15:19:27'),
(40,7,'2018-06-06 18:32:53'),
(41,7,'2018-06-06 18:56:17'),
(42,1,'2018-06-07 03:32:21'),
(43,2,'2018-06-07 04:41:04'),
(44,2,'2018-06-12 20:11:15');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `role` */

insert  into `role`(`id_role`,`role`) values 
(1,'Admin'),
(2,'Contributor'),
(3,'Registered User');

/*Table structure for table `ruangan` */

DROP TABLE IF EXISTS `ruangan`;

CREATE TABLE `ruangan` (
  `id_ruangan` int(11) NOT NULL AUTO_INCREMENT,
  `nomor_ruangan` varchar(25) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_ruangan`),
  KEY `nomor_ruangan` (`nomor_ruangan`),
  KEY `nomor_ruangan_2` (`nomor_ruangan`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `ruangan` */

insert  into `ruangan`(`id_ruangan`,`nomor_ruangan`,`kategori`) values 
(1,'AUD','Auditorium'),
(2,'GD516','Kelas Teori'),
(3,'GD714','Laboratorium Komputer'),
(4,'GD721','Kelas Teori'),
(5,'GD722','Kelas Teori'),
(6,'GD723','Laboratorium Komputer'),
(7,'GD912','Kelas Teori'),
(8,'GD913','Kelas Teori'),
(9,'GD914','Laboratorium Bahasa'),
(10,'GD923','Kelas Teori'),
(11,'GD924','Kelas Teori'),
(12,'GD925','Kelas Teori'),
(13,'GD927','Kelas Teori'),
(14,'GD928','Kelas Teori'),
(15,'GD929','Kelas Teori'),
(16,'GD933','Kelas Teori'),
(17,'GD934','Kelas Teori'),
(18,'GD935','Kelas Teori'),
(19,'GD937','Kelas Teori'),
(20,'GD938','Kelas Teori'),
(21,'GD939',''),
(22,'GD942',''),
(23,'GD943',''),
(24,'GD944',''),
(25,'GD513','Laboratorium Komputer'),
(26,'GD514','Laboratorium Komputer'),
(27,'GD515','Laboratorium Komputer'),
(28,'GD524','Laboratorium Komputer'),
(29,'GD525','Laboratorium Komputer'),
(30,'GD711','Laboratorium Komputer'),
(31,'GD712','Laboratorium Komputer'),
(32,'GD713','Laboratorium Komputer'),
(33,'GD724','Laboratorium Komputer'),
(34,'GD725','Laboratorium Komputer'),
(35,'GD726','Laboratorium Komputer'),
(36,'GD813','Laboratorium'),
(37,'GD814','Laboratorium'),
(38,'GD825','Laboratorium'),
(39,'GD826','Laboratorium'),
(40,'GD812','Laboratorium'),
(41,'LDTE','Laboratorium'),
(42,'LSK','Laboratorium'),
(43,'LSD','Laboratorium'),
(44,'GD916','Laboratorium');

/*Table structure for table `sesi` */

DROP TABLE IF EXISTS `sesi`;

CREATE TABLE `sesi` (
  `id_sesi` int(11) NOT NULL AUTO_INCREMENT,
  `sesi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_sesi`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `sesi` */

insert  into `sesi`(`id_sesi`,`sesi`) values 
(1,'T'),
(2,'P');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`created_at`,`updated_at`) values 
(1,'admin','SR0DMo-4FNgI-xQOHnWa0z6T8KJdU45F','$2y$13$XTd0jpLtKzdDme8ErIe1xug8ZjZ6EXxJ829836BKqj8l/cKIUh60a',NULL,'admin@stu.del.ac.id',10,1523943564,1523943564),
(2,'isabella','yCcUeV8bKJ4HmoYJLpX3-spqUJN3EiCy','$2y$13$R39xNezOVjk/94nBJ80LBeF.1CiHcZMBFL9QFiZTgAk8RLrP8QcnC',NULL,'isabella@stu.del.ac.id',10,1524010768,1524010768);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
